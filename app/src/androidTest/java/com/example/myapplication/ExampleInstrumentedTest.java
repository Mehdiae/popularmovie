package com.example.myapplication;



import android.arch.core.executor.testing.InstantTaskExecutorRule;


import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;


import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import org.junit.runner.RunWith;

import org.mockito.Mock;

import org.mockito.junit.MockitoJUnitRunner;


import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;




import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


@RunWith(MockitoJUnitRunner.class)
public class ExampleInstrumentedTest {

    private CountDownLatch lock = new CountDownLatch(1);

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();


    @Test
    public void testApiGetPopularList() {
        MutableLiveData<List<Movie>> data = new MutableLiveData<>();
        Observer<List<Movie>> observer = new Observer<List<Movie>>() {
            @Override
            public void onChanged(@Nullable List<Movie> movies) {
                assertTrue(!movies.isEmpty());
            }
        };
        data.observeForever(observer);
        MovieApi.getInstance().getAllMovies(data);

        try {
            lock.await(2000, TimeUnit.MILLISECONDS);
        }catch (InterruptedException e){

        }

        assertNotNull(data.getValue());
    }

    @Test
    public void testApiGetBitmap() {


        Observer<Bitmap> observer = new Observer<Bitmap>() {
            @Override
            public void onChanged(@Nullable Bitmap b) {
                assertTrue(b.getWidth()>0);
            }
        };
        Movie m = new Movie();
        m.setBackdropPath("/hpgda6P9GutvdkDX5MUJ92QG9aj.jpg");
        m.getLivaDataBitmap().observeForever(observer);
        MovieApi.getInstance().getPicture(m);

        try {
            lock.await(2000, TimeUnit.MILLISECONDS);
        }catch (InterruptedException e){

        }

        assertNotNull(m.getBitmap());
    }





}
