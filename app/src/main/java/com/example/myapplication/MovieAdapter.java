package com.example.myapplication;


import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.Observer;
import android.content.Context;


import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;





public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> {

    private List<Movie> data;

    private LayoutInflater layoutInflater;

    private RequestForImage requestForImage;
    private ItemClick itemClick;

    public MovieAdapter(Context context, RequestForImage req,ItemClick itemClick) {
        this.data = new ArrayList<>();
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.requestForImage = req;
        this.itemClick = itemClick;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.movie_item, parent, false);
        return new MovieViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        holder.bind(data.get(position));
        holder.onAppear();
    }



    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull MovieViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.onAppear();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull MovieViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.onDisappear();
    }

    public void setData(List<Movie> newData) {
        if (data != null) {
            MovieDiffCallback movieDiffCallback = new MovieDiffCallback(data, newData);
            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(movieDiffCallback);

            data.clear();
            data.addAll(newData);
            diffResult.dispatchUpdatesTo(this);
        } else {
            data = newData;
        }
    }

    class MovieViewHolder extends RecyclerView.ViewHolder implements LifecycleOwner {

        private ImageView imageView;
        private TextView title;
        private ConstraintLayout layout;
        private LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
        MovieViewHolder(View itemView) {
            super(itemView);
            lifecycleRegistry.markState(Lifecycle.State.INITIALIZED);
            lifecycleRegistry.markState(Lifecycle.State.CREATED);
            imageView = itemView.findViewById(R.id.image);
            title = itemView.findViewById(R.id.title);
            layout = itemView.findViewById(R.id.item);
        }

        void bind(final Movie movie) {
            if (movie != null) {
                title.setText(movie.getTitle());
                if (movie.getBitmap() == null) {
                    android.arch.lifecycle.Observer<Bitmap> observer = new Observer<Bitmap>() {
                        @Override
                        public void onChanged(@Nullable Bitmap o) {
                            imageView.setImageBitmap(o);
                        }
                    };
                    movie.getLivaDataBitmap().observe(this,observer);
                    requestForImage.onRequestForImage(movie);
                } else {
                    imageView.setImageBitmap(movie.getBitmap());
                }
                layout.setOnClickListener((view)->itemClick.onItemClicked(movie));


            }
        }


        public void onAppear() {
            lifecycleRegistry.markState(Lifecycle.State.RESUMED);
        }

        public void  onDisappear() {
            lifecycleRegistry.markState(Lifecycle.State.DESTROYED);
        }

        @NonNull
        @Override
        public Lifecycle getLifecycle() {
            return lifecycleRegistry;
        }
    }

    class MovieDiffCallback extends DiffUtil.Callback {

        private final List<Movie> oldMovies, newMovies;

        public MovieDiffCallback(List<Movie> oldM, List<Movie> newM) {
            this.oldMovies = oldM;
            this.newMovies = newM;
        }

        @Override
        public int getOldListSize() {
            return oldMovies.size();
        }

        @Override
        public int getNewListSize() {
            return newMovies.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldMovies.get(oldItemPosition).getId() == newMovies.get(newItemPosition).getId();
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return oldMovies.get(oldItemPosition).equals(newMovies.get(newItemPosition));
        }
    }

    public interface RequestForImage {
        void onRequestForImage(Movie movie);
    }
    public interface ItemClick {
        void onItemClicked(Movie movie);
    }
}