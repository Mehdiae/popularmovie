package com.example.myapplication;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;




public class MainActivity extends AppCompatActivity implements MovieAdapter.ItemClick {
    private MovieAdapter movieAdapter;
    private MovieViewModel movieViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        movieViewModel = ViewModelProviders.of(this).get(MovieViewModel.class);
        movieViewModel.getAllMovies().observe(this, movies -> movieAdapter.setData(movies));

        movieAdapter = new MovieAdapter(this,movieViewModel,this);

        RecyclerView recyclerView = findViewById(R.id.movielist);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(movieAdapter);

        movieViewModel.fetchMovie();


    }


    @Override
    public void onItemClicked(Movie movie) {
        Intent i = new Intent(this,DetailActivity.class);
        i.putExtra("movie",movie);

        startActivity(i);
    }
}
