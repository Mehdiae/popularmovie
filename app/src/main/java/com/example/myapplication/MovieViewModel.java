package com.example.myapplication;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;


import android.arch.lifecycle.MutableLiveData;

import android.support.annotation.NonNull;

import java.util.List;






public class MovieViewModel extends AndroidViewModel implements MovieAdapter.RequestForImage {

    private MovieApi api;
    private MutableLiveData<List<Movie>> movieListObservable =   new MutableLiveData<>();

    public MovieViewModel(@NonNull Application application) {
        super(application);

        api = MovieApi.getInstance();
    }


    public MutableLiveData<List<Movie>> getAllMovies(){

        return movieListObservable;
    }
    public void fetchMovie(){
        api.getAllMovies(  movieListObservable);
    }


    @Override
    public void onRequestForImage(Movie movie) {
        api.getPicture(movie);

    }



}
