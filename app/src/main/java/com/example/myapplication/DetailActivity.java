package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent i = getIntent();
        Movie movie = (Movie)i.getExtras().getParcelable("movie");

        ((ImageView)findViewById(R.id.imageView)).setImageBitmap(movie.getBitmap());
        ((TextView)findViewById(R.id.title)).setText(movie.getTitle());
        ((TextView)findViewById(R.id.rating)).setText(Float.toString(movie.getPopularity()));
        ((TextView)findViewById(R.id.overview)).setText(movie.getOverview());
    }
}
