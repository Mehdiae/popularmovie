package com.example.myapplication;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import java.util.List;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Url;

public class MovieApi {
    private static final Object sLock = new Object();
    private static MovieApi INSTANCE;
    private static MovieDBApiInterface apiService;
    public static MovieApi getInstance() {
        synchronized (sLock) {
            if (INSTANCE == null) {
                Gson gson = new GsonBuilder()
                        .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                        .create();
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("https://image.tmdb.org/")
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();
                apiService = retrofit.create(MovieDBApiInterface.class);
                INSTANCE = new MovieApi();

            }
            return INSTANCE;
        }
    }


    public int getAllMovies(MutableLiveData<List<Movie>> data){

        Call<Popular> call = apiService.getPopular("https://api.themoviedb.org/3/movie/popular?api_key=c06e14cd13b2c6373fdc8f9f3dd47eb3");
        call.enqueue(new Callback<Popular>() {
            @Override
            public void onResponse(Call<Popular> call, final Response<Popular> response) {
                if(response.body()!=null && response.body().getMovies()!=null && !response.body().getMovies().isEmpty()){
                    data.setValue(response.body().getMovies());
                }
            }

            @Override
            public void onFailure(Call<Popular> call, Throwable t) {

            }
        });
        return 0;
    }

    public void getPicture(Movie m){


        Call<ResponseBody> call = apiService.fetchPicture(m.getBackdropPath().substring(1));
        Log.d("image",call.request().url().toString());

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Bitmap bmp = BitmapFactory.decodeStream(response.body().byteStream());
                        m.setBitmap(bmp);

                    } else {
                        Log.d("image","empty resp");
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("image","failure");
            }
        });

    }




    private interface MovieDBApiInterface {

        @GET
        Call<Popular> getPopular(@Url String key);
        @GET("t/p/w300/{image}")
        Call<ResponseBody> fetchPicture(@Path("image") String url);
    }
}
